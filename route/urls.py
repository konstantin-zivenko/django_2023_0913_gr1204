from django.urls import path, register_converter, re_path

from route import views
from route.route_conv import FourDigitConv

register_converter(FourDigitConv, "yyyy")

urlpatterns = (
    path("", views.index, name="index"),
    path("articles/2003/", views.special_case_2003, name="special_2003"),
    path("articles/<yyyy:year>/", views.year_archive),
    re_path(r"^articles/(?P<year>[0-9]{4})/(?P<month>[01]?[0-9]{0,1})/$", views.month_archive),
    path("articles/<int:year>/<int:month>/<slug:slug>/", views.article_detail),
)

app_name = "route"

# str - виключає - "/"
# int - 0 ....
# slug - a..zA..Z0..0_-
# uuid -
# path - /
