from django.http import HttpResponse, HttpRequest


def index(request: HttpRequest) -> HttpResponse:
    return HttpResponse("it is a main page...")


def special_case_2003(request: HttpRequest) -> HttpResponse:
    return HttpResponse("it is special case 2003")


def year_archive(request: HttpRequest, year: int) -> HttpResponse:
    return HttpResponse(f"it is years {year} archive request...")


def month_archive(request: HttpRequest, year: int, month: int) -> HttpResponse:
    return HttpResponse(f"article for {year} year and {month} month")


def article_detail(request: HttpRequest, year: int, month: int, slug: str) -> HttpResponse:
    return HttpResponse(f"article for {year} year and {month} month title: {slug}")
