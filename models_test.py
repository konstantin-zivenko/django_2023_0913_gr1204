from books.models import Book, Publisher, Author, Tag, Genre


def main():
    genre = Genre(
        name="novel"
    )
    genre.save()
    genre_2 = Genre(name="scifi")
    genre_2.save()
    pub = Publisher(name="Ranok")
    pub.save()
    pub_2 = Publisher(name="ThomsonReuters")
    pub_2.save()


if __name__ == "__main__":
    main()

