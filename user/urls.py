from django.urls import path, include
from user.views import dashboard, MyPasswordChangeView, MyPasswordChangeDoneView, MyLoginView, MyPasswordResetView, \
    MyPasswordResetDoneView, MyPasswordResetConfirmView, MyPasswordResetCompleteView, register

urlpatterns = [
    path("dashboard/", dashboard, name="dashboard"),
    path(
        "accounts/password_change/", MyPasswordChangeView.as_view(), name="password_change"
    ),
    path(
        "accounts/password_change/done/",
        MyPasswordChangeDoneView.as_view(),
        name="password_change_done",
    ),
    path("accounts/login/", MyLoginView.as_view(), name="login"),
    path("accounts/password_reset/", MyPasswordResetView.as_view(), name="password_reset"),
    path(
        "accounts/password_reset/done/",
        MyPasswordResetDoneView.as_view(),
        name="password_reset_done",
    ),

    path(
        "accounts/reset/<uidb64>/<token>/",
        MyPasswordResetConfirmView.as_view(),
        name="password_reset_confirm",
    ),
    path(
        "accounts/reset/done/",
        MyPasswordResetCompleteView.as_view(),
        name="password_reset_complete",
    ),
    path("accounts/", include("django.contrib.auth.urls")),
    path('accounts/register', register, name='register'),
]

app_name = "user"
