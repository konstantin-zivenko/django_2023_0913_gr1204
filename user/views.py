from django.contrib.auth import login
from django.shortcuts import render, redirect
from django.contrib.auth.views import PasswordChangeView, PasswordChangeDoneView, LoginView, PasswordResetView, \
    PasswordResetDoneView, PasswordResetConfirmView, PasswordResetCompleteView
from django.urls import reverse_lazy, reverse


from user.forms import CustomerUserCreateForm

def dashboard(request):
    return render(request, "user/dashboard.html")


class MyPasswordChangeView(PasswordChangeView):
    success_url = reverse_lazy("user:password_change_done")
    template_name = "registration/user/password_change_form.html"


class MyPasswordChangeDoneView(PasswordChangeDoneView):
    template_name = "registration/user/password_change_done.html"


class MyLoginView(LoginView):
    template_name = "registration/user/login.html"


class MyPasswordResetView(PasswordResetView):
    email_template_name = "registration/user/password_reset_email.html"
    subject_template_name = "registration/password_reset_subject.txt"
    success_url = reverse_lazy("user:password_reset_done")
    template_name = "registration/user/password_reset_form.html"


class MyPasswordResetDoneView(PasswordResetDoneView):
    template_name = "registration/user/password_reset_done.html"


class MyPasswordResetConfirmView(PasswordResetConfirmView):
    success_url = reverse_lazy("user:password_reset_complete")
    template_name = "registration/user/password_reset_confirm.html"


class MyPasswordResetCompleteView(PasswordResetCompleteView):
    template_name = "registration/user/password_reset_complete.html"


def register(request):
    if request.method == "GET":
        return render(
            request,
            "user/register.html",
            {"form": CustomerUserCreateForm}
        )
    elif request.method == "POST":
        form = CustomerUserCreateForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect(reverse("user:dashboard"))
