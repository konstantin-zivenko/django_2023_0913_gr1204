from django.contrib.auth.forms import UserCreationForm

from user.models import User


class CustomerUserCreateForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = User
        fields = UserCreationForm.Meta.fields + ("email", )
