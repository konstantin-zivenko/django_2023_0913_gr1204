django, gr/1204

---

[link to diagram](https://drive.google.com/file/d/1KrhLGkKGMLIeDuNi3pB5LZI0xx6ZqSpV/view?usp=sharing)

[video 1](https://youtu.be/31VOX19nOsY) - intro, settings, MVT, first models

[video 2](https://youtu.be/yQbsH2jPcJE) - models and fields

[video 3](https://youtu.be/Z25kf2ff9jo) fields, models, query

[video 4](https://youtu.be/Gb7kc7050fY) - urlconf

[video 5](https://youtu.be/yBUWvhEZOU8) - polls, templates intro

[video 6](https://youtu.be/WupmH5FhT7Y)

[video 7](https://youtu.be/Wmqec1Ks5Zo) - generic views, forms

[video 8](https://youtu.be/-TuyEjVIUYU) - forms and start user management

[video 9](https://youtu.be/pZ5OwZOBQ1k) - user management

[video 10](https://youtu.be/9nwwyKS_pOc) - user management? part 2

