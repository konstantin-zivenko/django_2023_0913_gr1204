from django.urls import path
from moviefacts import views

app_name = "moviefacts"

urlpatterns = [
    path("citizen_kane/", views.citizen_kane, name="citizen_kane"),
    path("casablanca/", views.casablanca, name="casablanca"),
    path("falcon/", views.maltese_falcon, name="falcon"),
    path("psycho/", views.psycho, name="psycho"),
    path("listing/", views.listing, name="listing"),
]